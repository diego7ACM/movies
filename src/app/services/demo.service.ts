import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import{iDemo} from '../model/iDemo.interface';

@Injectable({
  providedIn: 'root'
})
export class DemoService {

private url: string='';
private apiKey: string = 'a5517cca';

  constructor( private http: HttpClient) { }

  searchMovies(title:string, type:string){
this.url=`http://www.omdbapi.com/?s=${encodeURI(title)}&type=${type}&apikey=${this.apiKey}`;
console.log(this.url);
return this.http.get<iDemo>(this.url).pipe(map(results=>results['Search']));
  }
  getDetails(id:string){
return  this.http.get<iDemo>(`http://www.omdbapi.com/?i=${id}&plot=full&apiKey=${this.apiKey}`);
  }
}
