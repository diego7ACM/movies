import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {DemoService} from '../../services/demo.service';
import {iDemo} from '../../model/iDemo.interface';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
results: Observable<iDemo>;
term:string='';
type:string='';
  constructor(private demoService:DemoService) { }

  ngOnInit() {
  }
searchChanged(): void{
  this.results=this.demoService.searchMovies(this.term,this.type);
}
}
