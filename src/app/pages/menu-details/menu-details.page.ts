import { Component, OnInit } from '@angular/core';
import {DemoService} from '../../services/demo.service';
import { ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-menu-details',
  templateUrl: './menu-details.page.html',
  styleUrls: ['./menu-details.page.scss'],
})
export class MenuDetailsPage implements OnInit {
content: object=null;
  constructor(private demoService: DemoService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let id=this.activatedRoute.snapshot.paramMap.get('id');
    this.demoService.getDetails(id).subscribe(result=>this.content=result);
  }

}
